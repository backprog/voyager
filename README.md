# Voyager

## Deployment
1. Just run npm install in the root folder to install all dependencies.
2. Create a locals.js file in the config folder and paste the following:
        `
        module.exports = {
            mapsApiKey: 'your google map key'
        };
        `
3. Run the app with: 
    `node app.js`

#### Dev
Alternatively, sails can be installed globally with:
    `npm install sails -g`   
And the app started with:
    `sails lift`   
The app will listen to port 1337 by default