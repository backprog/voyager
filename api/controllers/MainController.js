/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    index: function (req, res) {

        res.view('index');
    },

    getAddress: function (req, res) {

        try {

            var request = require('request');

            if(req.query.hasOwnProperty('lat') && req.query.hasOwnProperty('lng')) {

                var latlng = req.query.lat + ',' + req.query.lng,
                    url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ latlng +"&sensor=true";

                request.get(url, function (err, response) {

                    if(!err && response) {

                        try {

                            res.json(JSON.parse(response.body));
                        }
                        catch(e) {

                            console.log(e);
                            res.serverError();
                        }
                    }
                    else {

                        res.serverError();
                    }
                });
            }
            else {

                res.badRequest();
            }
        }
        catch(e) {

            console.log(e);
            res.serverError();
        }
    },

    getCoordinates: function (req, res) {

        var request = require('request');

        if(req.query.hasOwnProperty('address')) {

            var address = req.query.address,
                url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + sails.config.mapsApiKey;

            request.get(url, function (err, response) {

                if(!err && response) {

                    try {

                        res.json(JSON.parse(response.body));
                    }
                    catch(e) {

                        console.log(e);
                        res.serverError();
                    }
                }
                else {

                    res.serverError();
                }
            });
        }
        else {

            res.badRequest();
        }
    },

    getDirection: function (req, res) {

        var request = require('request');

        if(req.query.hasOwnProperty('origin') && req.query.hasOwnProperty('destination')) {

            var origin = req.query.origin, destination = req.query.destination,
                url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&key=" + sails.config.mapsApiKey;

            request.get(url, function (err, response) {

                if(!err && response) {

                    try {

                        res.json(JSON.parse(response.body));
                    }
                    catch(e) {

                        console.log(e);
                        res.serverError();
                    }
                }
                else {

                    res.serverError();
                }
            });
        }
        else {

            res.badRequest();
        }
    }
};

