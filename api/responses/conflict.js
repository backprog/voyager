/**
 * 409 (Conflict Error) Response
 *
 * Usage:
 * return res.conflict();
 * return res.conflict(redirect);
 *
 */

module.exports = function serverError(redirect) {

    // Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;
    var sails = req._sails;

    // Set status code
    res.status(409);

    if (req.isSocket)
        return res.send({error: true});

    if (/\./.test(req.path))
        return res.send('');

    if (redirect)
        return res.redirect(redirect);

    return res.view('409');
};

