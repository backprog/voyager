/**
 * 403 (Forbidden) Handler
 *
 * Usage:
 * return res.forbidden();
 * return res.forbidden(redirect);
 *
 */

module.exports = function forbidden(redirect) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(403);

  if (req.isSocket)
    return res.send({error: true});

  if (/\./.test(req.path))
    return res.send('');

  if(redirect)
    return res.redirect(redirect);

  return res.view('403');
};

