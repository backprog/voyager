/**
 * 401 (Not authenticated Error) Response
 *
 * Usage:
 * return res.notAuthenticated();
 * return res.notAuthenticated(redirect);
 *
 */

module.exports = function notAuthenticated(redirect) {

    // Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;
    var sails = req._sails;

    // Set status code
    res.status(401);

    if (req.isSocket)
        return res.send({ success:true });

    if (/\./.test(req.path))
        return res.send('');

    if (redirect)
        return res.redirect(redirect);

    return res.view('401');
};
