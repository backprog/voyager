/**
 * 400 (Bad Request) Handler
 *
 * Usage:
 * return res.badRequest();
 * return res.badRequest(redirect);
 *
 */

module.exports = function badRequest(redirect) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(400);

  if (req.isSocket)
    return res.send({error: true});

  if (/\./.test(req.path))
    return res.send('');

  if (redirect)
    return res.redirect(redirect);

  return res.view('400');
};

