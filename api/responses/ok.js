/**
 * 200 (OK) Response
 *
 * Usage:
 * return res.ok();
 * return res.ok(data);
 * return res.ok(data, 'auth/login');
 *
 */

module.exports = function sendOK(redirect) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(200);

  if (req.isSocket)
    return res.send({ success:true });

  if (/\./.test(req.path))
    return res.send('');

  if (redirect)
    return res.redirect(redirect);

  return res.send('OK');
};
