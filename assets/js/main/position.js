/**
 *
 * @param parent
 * @constructor
 *
 * Position object to geolocate the user and make requests to google maps API.
 */

var Position = function (parent) {

    this.parent = parent;
    this.geolocate = false;
    this.current_position = {};
    this.destination_position = {};
    this.directions = null;
    this.origin = null;
    this.destination = null;
    this.initTimeout = 0;
};

// Geolocate the user
Position.prototype.geoLocation = function (callback) {

    // If geolocation is available
    if(navigator.geolocation) {

        var timeout = function () {

            if(callback && typeof callback == 'function')
                callback();
        };

        navigator.geolocation.getCurrentPosition(this.setOriginLocation.bind(this, callback), this.onError.bind(this, callback), { enableHighAccuracy:true });

        this.initTimeout = setTimeout(timeout.bind(this), 3000);
    }
    else {

        if(callback && typeof callback == 'function')
            callback();
    }
};

// Set the user origin coordinates
Position.prototype.setOriginLocation = function (callback, position) {

    if(this.initTimeout)
        clearTimeout(this.initTimeout);

    if(typeof position == 'object' && typeof position.coords != 'undefined') {

        this.current_position = position.coords;
        this.geolocate = true;
    }

    if(callback && typeof callback == 'function')
        callback();
};

// Set the user destination coordinates
Position.prototype.setDestinationLocation = function (callback, position) {

    if(typeof position == 'object' && typeof position.coords != 'undefined')
        this.destination_position = position.coords;

    if(callback && typeof callback == 'function')
        callback();
};

// Set the user origin address
Position.prototype.setOrigin = function (val) {

    this.origin = val;
};

// Set the user destination address
Position.prototype.setDestination = function (val) {

    this.destination = val;
};

// Set the directions response
Position.prototype.setDirections = function (val) {

    this.directions = val;
};

// Get the user location from the coordinates
Position.prototype.getAddress = function (callback) {

    try {

        var lat = this.current_position.latitude,
            lng = this.current_position.longitude;

        // GET request to retrieve the user current location
        io.socket.get('/api/address?lat=' + lat + '&lng=' + lng, function (res, jwres) {

            if(jwres.statusCode == 200) {

                if(res.status == 'OK' && res.hasOwnProperty('results') && res.results.length > 0) // Found results
                    callback(null, res.results);
                else // No results found
                    callback(404, null);
            }
            else {

                callback(jwres.statusCode, null);
            }
        });
    }
    catch(e) {

        console.log(e);
        callback(e, null);
    }
};

// Get the user coordinates from the address
Position.prototype.getCoordinates = function (address, callback) {

    try {

        // GET request to retrieve the user current coordinates
        io.socket.get('/api/coordinates?address=' + encodeURIComponent(address), function (res, jwres) {

            if(jwres.statusCode == 200) {

                if(res.status == 'OK' && res.hasOwnProperty('results') && res.results.length > 0) // Found results
                    callback(null, res.results);
                else // No results found
                    callback(404, null);
            }
            else {

                callback(jwres.statusCode, null);
            }
        });
    }
    catch(e) {

        console.log(e);
        callback(e, null);
    }
};

// Get the navigation steps
Position.prototype.getDirection = function (callback) {

    if(this.origin && this.destination) {

        var origin = encodeURIComponent(this.origin),
            destination = encodeURIComponent(this.destination);

        try {

            io.socket.get('/api/direction?origin=' + origin + '&destination=' + destination, function (res, jwres) {

                if(jwres.statusCode == 200) {

                    if(res.status == 'OK' && res.hasOwnProperty('routes') && res.routes.length > 0) // Found results
                        callback(null, res);
                    else // No results found
                        callback(404, null);
                }
                else {

                    callback(jwres.statusCode, null);
                }
            });
        }
        catch(e) {

            console.log(e);
            callback(e, null);
        }
    }
    else {

        var missing = (!this.origin) ? "current location" : "destination";

        callback("Missing " + missing, null);
    }
};

// Geolocation errors handler
Position.prototype.onError = function (callback, err) {

    if(this.initTimeout)
        clearTimeout(this.initTimeout);

    console.log(err);

    switch(err.code) {
        case 2: toastr.error("We were not able to determine your position"); break;
        case 3: toastr.error("We ran out of time to determine your position"); break;
    }

    if(callback && typeof callback == 'function')
        callback();
};