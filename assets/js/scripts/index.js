
var Main = function () {

    // Instantiate position handler
    this.position_handler = new Position(this);

    // Try to geo locate the user
    this.position_handler.geoLocation(this.init.bind(this));
};

// Start the app
Main.prototype.init = function () {

    this.adjustDisplay()
        .onResize()
        .controls()
        .prompts();
};

// Prompts init
Main.prototype.prompts = function () {

    if(!$.modal.isActive()) {

        if(this.position_handler.geolocate) // If the user allowed geolocation and the navigator support it
            this.promptGeoLocation();
        else // Fallback to default behavior
            this.promptLocation();
    }

    return this;
};

// Modal window to confirm geolocated address
Main.prototype.promptGeoLocation = function () {

    // Position callback
    var done = function (err, address) {

        if(!err) {

            try {

                var $modal = $('#modal'), $content;

                // Get the html template
                $content = $(JST['assets/templates/prompt-geolocation-item.html']());

                // Replace address part
                $content.find('h3').text(address[0].formatted_address);

                // Insert the html in the modal
                $modal.children('div').html($content);

                // Open the modal window
                $modal.modal({
                    clickClose:false,
                    closeExisting: true,
                    escapeClose: false,
                    showClose: false
                });
            }
            catch(e) {

                console.log(e);
                this.promptLocation();
            }
        }
        else {

            this.promptLocation();
        }
    };

    // Buttons callback
    var listener = function (evt) {

        evt.preventDefault();

        if($(evt.target).hasClass('confirm')) { // If the address is correct

            this.position_handler.setOrigin($('#modal').find('#prompt-geolocation').find('h3').text());
            this.promptDestination();
        }
        else { // Fallback to default behavior

            this.promptLocation();
        }
    };

    // Get address from latitude & longitude
    this.position_handler.getAddress(done.bind(this));

    // Modal buttons click event listener
    $(document).off('click', '#modal #prompt-geolocation a').on('click', '#modal #prompt-geolocation a', listener.bind(this));
};

// Modal window to submit current location
Main.prototype.promptLocation = function () {

    var $modal = $('#modal'), $content;

    // Position coordinates callback
    var onCoords = function (err, results) {

        if(!err) {

            try {

                var coordinates = {
                    coords: {
                        latitude: results[0].geometry.location.lat,
                        longitude: results[0].geometry.location.lng
                    }
                };

                // Set the origin address
                this.position_handler.setOrigin($('#modal').find('#prompt-location').find('input[type=text]').val().trim());

                // Set the origin's coordinates values
                this.position_handler.setOriginLocation(this.promptDestination.bind(this), coordinates);
            }
            catch(e) {

                console.log(e);
                toastr.error("An error occurred. Please try again.");

                // Reset the button to its default
                $('#modal').find('#prompt-destination form button').html('Send');
            }
        }

        else {

            switch(err) {

                case 404: toastr.error("We were not able to find your location. Is the address correct ?"); break;
                default: toastr.error("An error occurred. Please try again.");
            }

            // Reset the button to its default
            $('#modal').find('#prompt-location form button').html('Send');
        }
    };

    // Form submit callback
    var listener = function (evt) {

        evt.preventDefault();

        var $button = $(evt.target).find('button');

        // If the form is not in submit process
        if($button.children('i').length == 0) {

            // Get the input value
            var address = $('#modal').find('#prompt-location').find('input[type=text]').val().trim();

            // Validating the address by getting his coordinates
            if(address) {

                // Add loader
                $button.html('<i class="fa fa-spin fa-spinner"></i>');

                this.position_handler.getCoordinates(address, onCoords.bind(this));
            }
            else {

                toastr.error("A valid address is required");
            }
        }
    };

    // Get the html template
    $content = $(JST['assets/templates/prompt-location-item.html']());

    // Insert the html in the modal
    $modal.children('div').html($content);

    // Open the modal window
    $modal.modal({
        clickClose:false,
        closeExisting: true,
        escapeClose: false,
        showClose: false
    });

    // Set focus on the input
    $modal.find('input:first').focus();

    // Modal form event listener
    $(document).off('submit', '#modal #prompt-location form').on('submit', '#modal #prompt-location form', listener.bind(this));
};

// Modal window to submit destination location
Main.prototype.promptDestination = function () {

    var $modal = $('#modal'), $content;

    // Position coordinates callback
    var onCoords = function (err, results) {

        if(!err) {

            try {

                var coordinates = {
                    coords: {
                        latitude: results[0].geometry.location.lat,
                        longitude: results[0].geometry.location.lng
                    }
                };

                // Set the destination address
                this.position_handler.setDestination($('#modal').find('#prompt-destination').find('input[type=text]').val().trim());

                // Set the destination's coordinates values
                this.position_handler.setDestinationLocation(this.drawDirection.bind(this), coordinates);
            }
            catch(e) {

                console.log(e);
                toastr.error("An error occurred. Please try again.");

                // Reset the button to its default
                $('#modal').find('#prompt-destination form button').html('Send');
            }
        }
        else {

            switch(err) {

                case 404: toastr.error("We were not able to find your destination. Is the address correct ?"); break;
                default: toastr.error("An error occurred. Please try again.");
            }

            // Reset the button to its default
            $('#modal').find('#prompt-destination form button').html('Send');
        }
    };

    // Form submit callback
    var listener = function (evt) {

        evt.preventDefault();

        var $button = $(evt.target).find('button');

        // If the form is not in submit process
        if($button.children('i').length == 0) {

            // Get the input value
            var address = $('#modal').find('#prompt-destination').find('input[type=text]').val().trim();

            // Validating the address by getting his coordinates
            if(address) {

                // Add loader
                $button.html('<i class="fa fa-spin fa-spinner"></i>');

                this.position_handler.getCoordinates(address, onCoords.bind(this));
            }
            else {

                toastr.error("A valid address is required");
            }
        }
    };

    // Get the html template
    $content = $(JST['assets/templates/prompt-destination-item.html']());

    // Insert the html in the modal
    $modal.children('div').html($content);

    // Open the modal window
    $modal.modal({
        clickClose:false,
        closeExisting: true,
        escapeClose: false,
        showClose: false
    });

    // Set focus on the input
    $modal.find('input:first').focus();

    // Modal form event listener
    $(document).off('submit', '#modal #prompt-destination form').on('submit', '#modal #prompt-destination form', listener.bind(this));
};

// Set navigation list
Main.prototype.drawDirection = function () {

    // Position direction callback
    var onDirection = function (err, res) {

        if(!err && res) {

            this.position_handler.setDirections(res);
            $.modal.close();

            try {

                var steps = res.routes[0].legs[0].steps,
                    i = 0, l = steps.length,
                    frag = document.createDocumentFragment(),
                    $locations = $('#locations'),
                    $item;

                $locations.children('p:first').children('span').text(this.position_handler.origin);
                $locations.children('p:last').children('span').text(this.position_handler.destination);
                $locations.show();

                // Loop on each steps and add a row
                for(; i < l; i++) {

                    // Get the html template
                    $item = $(JST['assets/templates/chat-item.html']());

                    // If the maneuver attribute is set, add the appropriate class to the first span
                    if(steps[i].hasOwnProperty('maneuver'))
                        $item.children('span:first').addClass(steps[i].maneuver);
                    else
                        $item.children('span:first').addClass('compass');

                    // Add the instructions to the last span
                    $item.children('span:last').html(steps[i].html_instructions);

                    frag.appendChild($item[0]);
                }

                // Add the list of steps in the container
                $('#chat').children('ul').html(frag);

                this.onResize();
                this.setMap();
            }
            catch(e) {

                console.log(e);
                toastr.error("Something went wrong while calculating your routes. Please try again");
            }
        }
        else {

            switch(err) {

                case 404: toastr.error("We were not able to find a way to drive to " + this.position_handler.destination); break;
                default: toastr.error("An error occurred. Please try again.");
            }
        }
    };

    // Get the navigation object
    this.position_handler.getDirection(onDirection.bind(this));
};

// Set the map
Main.prototype.setMap = function () {

    var pos = this.position_handler,
        directionsService = new google.maps.DirectionsService(),
        directionsDisplay = new google.maps.DirectionsRenderer(),
        request = {
            origin: new google.maps.LatLng(pos.current_position.latitude, pos.current_position.longitude),
            destination: new google.maps.LatLng(pos.destination_position.latitude, pos.destination_position.longitude),
            travelMode: google.maps.TravelMode.DRIVING
        },
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(32.0833299, 34.7929448)
        });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {

        if (status == google.maps.DirectionsStatus.OK) {

            directionsDisplay.setDirections(response);
            directionsDisplay.setMap(map);
        }
        else {

            toastr.error("Directions request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed");
        }
    });
};

// Controls events binding
Main.prototype.controls = function () {

    // Map button callback
    var onMap = function (evt) {

        evt.preventDefault();

        // If there's no active location prompt
        if(!$.modal.isActive()) {

            var $map = $('#map-container');

            // If the map is not visible
            if($map.hasClass('invisible'))
                $map.removeClass('invisible');
            else
                $map.addClass('invisible');
        }
    };

    // Reset button callback
    var onReset = function (evt) {

        evt.preventDefault();

        if($.modal.isActive()) { // Cancel the location prompt if he's active

            $.modal.close();
        }
        else { // Else close the map and open the location prompt

            $('#map-container').addClass('invisible');

            this.init();
        }
    };

    // Map button
    $(document).off('click', '#controls .fa-map').on('click', '#controls .fa-map', onMap.bind(this));

    // Reset button
    $(document).off('click', '#controls .fa-repeat').on('click', '#controls .fa-repeat', onReset.bind(this));

    return this;
};

// Callback of the resize event
Main.prototype.onResize = function () {

    $('#chat').height($(window).height() - $('#locations').outerHeight(true) - $('#controls').outerHeight(true));

    return this;
};

// Bind function to the resize event
Main.prototype.adjustDisplay = function () {

    var id = 0;
    var resize = function () {

        if(id)
            clearTimeout(id);

        id = setTimeout(this.onResize.bind(this), 200);
    };

    $(window).off('resize').on('resize', resize.bind(this));

    return this;
};

$(document).ready(function () {
    new Main();
});